package org.bitbucket.cpointe.fermenter.agitator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import javax.inject.Inject;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.bitbucket.fermenter.mda.GenerateSourcesHelper;
import org.bitbucket.fermenter.mda.PackageManager;
import org.bitbucket.fermenter.mda.element.ExpandedProfile;
import org.bitbucket.fermenter.mda.element.Target;
import org.bitbucket.fermenter.mda.generator.GenerationContext;
import org.bitbucket.fermenter.mda.generator.Generator;
import org.bitbucket.fermenter.mda.metamodel.*;
import org.bitbucket.fermenter.mda.util.MessageTracker;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.FileSystemOperations;
import org.gradle.api.logging.Logger;
import org.gradle.api.tasks.Nested;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.util.PatternSet;

/**
 * Gradle task that leverages Fermenter to load, validate, and process specified
 * metamodels, and appropriately generate sources, resources, and other files
 * based on the specified {@link org.bitbucket.fermenter.mda.element.Profile}.
 */
public class GenerateSourcesTask extends DefaultTask {

    private VelocityEngine velocityEngine;
    private FermenterPluginExtension extension;

    private Map<String, ExpandedProfile> profiles = new HashMap<>();
    private Map<String, Target> targets = new HashMap<>();

    private FileSystemOperations fileSystemOperations;

    /**
     * Expected filename containing Fermenter generation targets definitions.
     */
    protected static final String TARGETS_FILENAME = "targets.json";

    /**
     * Expected filename containing Fermenter profiles defining logical
     * groupings of generation targets.
     */
    protected static final String PROFILES_FILENAME = "profiles.json";

    /**
     * Uses the project's name as an identifying ID for Fermenter metamodel
     * loading
     */
    private String artifactId = getProject().getName();

    @Inject
    public GenerateSourcesTask(FileSystemOperations fileSystemOperations) {
        this.velocityEngine = new VelocityEngine();
        this.velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADERS, "classpath");
        this.velocityEngine.setProperty("resource.loader.classpath.class", ClasspathResourceLoader.class.getName());
        this.velocityEngine.init();

        this.fileSystemOperations = fileSystemOperations;
    }

    /**
     * Retrieves the {@link FermenterPluginExtension} which contains all (lazy)
     * configurations exposed to developers, such as directories in which source
     * files will be generated, which generation profile to use, etc.
     * <b>NOTE:</b> As this accessor method is marked with {@link Nested}, all
     * appropriately marked lazy attributes within
     * {@link FermenterPluginExtension} will be utilized by Gradle to
     * participate in task avoidance and dependency graph activities.
     * 
     * @return
     */
    @Nested
    public FermenterPluginExtension getExtension() {
        return this.extension;
    }

    /**
     * Injects the developer provided {@link FermenterPluginExtension} to
     * configure this task's execution.
     * 
     * @param extension
     */
    protected void setExtension(FermenterPluginExtension extension) {
        this.extension = extension;
    }

    /**
     * Helper accessor that retrieves the configured base location for
     * generated, modifiable files from the given
     * {@link FermenterPluginExtension} as a {@link File}.
     *
     * @return
     */
    private File getMainSourceRootAsFile() {
        return getExtension().getMainSourceRoot().getAsFile().get();
    }

    /**
     * Helper accessor that retrieves the configured base package to apply to
     * relevant generated source files.
     * 
     * @return
     */
    private String getBasePackageAsString() {
        return getExtension().getBasePackage().get();
    }

    /**
     * Loads the specified metamodels and appropriately generates sources,
     * resources, and other files.
     */
    @TaskAction
    public void generateFermenterSources() {
        GenerateSourcesHelper.suppressKrauseningWarnings();
        // First, delete the base location at which generated, non-modifiable
        // files are created
        this.fileSystemOperations.delete(deleteSpec -> {
            DirectoryProperty generatedSourceRoot = getExtension().getGeneratedSourceRoot();
            getLogger().info("Deleting generated source files at {}", generatedSourceRoot.get().toString());
            deleteSpec.delete(generatedSourceRoot);
        });

        // As Gradle uses a daemon to execute build tasks, errors and warnings
        // from previous build executions may be stored in the MessageTracker
        // singleton, so preemptively clear them before loading any metamodels
        MessageTracker.getInstance().clear();
        loadTargets();
        loadProfiles();
        try {
            loadAndValidateMetamodels();
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException
                | IllegalAccessException | MalformedURLException e) {
            throw new GradleException("Could not load and validate metamodel repository", e);
        }
        performSourcesGeneration();
    }

    /**
     * Loads all Fermenter generation {@link Target}s that are specified in
     * {@link #TARGETS_FILENAME} files via either direct file references or
     * dependencies specified in the
     * {@link FermenterPlugin#PROFILES_AND_TARGETS_CONFIGURATION} configuration.
     */
    protected void loadTargets() {
        Configuration profilesAndTargetsConf = getProject().getConfigurations()
                .getByName(FermenterPlugin.PROFILES_AND_TARGETS_CONFIGURATION);

        Consumer<InputStream> loadTargetsFromStream = loadFromStream -> {
            try {
                GenerateSourcesHelper.loadTargets(loadFromStream, targets);
            } catch (IOException e) {
                throw new InvalidUserDataException(String.format("Could not parse %s", TARGETS_FILENAME), e);
            }
        };

        loadFromFilesInConfiguration(profilesAndTargetsConf, TARGETS_FILENAME, loadTargetsFromStream);
        loadFromJarsInConfiguration(profilesAndTargetsConf, TARGETS_FILENAME, loadTargetsFromStream);
    }

    /**
     * Loads all Fermenter generation
     * {@link org.bitbucket.fermenter.mda.element.Profile}s that are specified
     * in {@link #PROFILES_FILENAME} files via either direct file references or
     * dependencies specified in the
     * {@link FermenterPlugin#PROFILES_AND_TARGETS_CONFIGURATION} configuration.
     */
    protected void loadProfiles() {
        Configuration profilesAndTargetsConf = getProject().getConfigurations()
                .getByName(FermenterPlugin.PROFILES_AND_TARGETS_CONFIGURATION);

        Consumer<InputStream> loadProfilesFromStream = loadFromStream -> {
            try {
                GenerateSourcesHelper.loadProfiles(loadFromStream, profiles);
            } catch (IOException e) {
                throw new InvalidUserDataException(String.format("Could not parse %s", PROFILES_FILENAME), e);
            }
        };

        loadFromFilesInConfiguration(profilesAndTargetsConf, PROFILES_FILENAME, loadProfilesFromStream);
        loadFromJarsInConfiguration(profilesAndTargetsConf, PROFILES_FILENAME, loadProfilesFromStream);

        for (ExpandedProfile p : profiles.values()) {
            p.dereference(profiles, targets);
        }
    }

    /**
     * Loads all metamodels into the configured {@link ModelInstanceRepository}
     * implementation and validates all metamodels. Currently, this method looks
     * for any metamodels that may have been defined using the legacy XML-based
     * format and performs any necessary conversions to upgrade the metamodels.
     * In future iterations, this legacy migration functionality will be
     * deprecated and removed.
     *
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws MalformedURLException
     */
    protected void loadAndValidateMetamodels() throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException, MalformedURLException {
        ModelRepositoryConfiguration config = createMetamodelRepositoryConfiguration();

        // first load the legacy repository:
        ModelInstanceRepository legacyRepository = GenerateSourcesHelper.loadLegacyMetadataRepository(config,
                gradleLoggerDelegate);

        // This is a stand-in to prevent NPEs for some optional functionality
        // that we want in the end product,
        // but doesn't matter for the conversion:
        ModelInstanceRepositoryManager.setRepository(new DefaultModelInstanceRepository(config));

        LegacyMetadataConverter converter = new LegacyMetadataConverter();
        converter.convert(artifactId, getBasePackageAsString(), getMainSourceRootAsFile());

        // then load the new repository:
        ModelInstanceRepository newRepository = GenerateSourcesHelper.loadMetadataRepository(config,
                getExtension().getMetadataRepositoryImpl().get(), gradleLoggerDelegate);

        long start = System.currentTimeMillis();
        getLogger().info("START: validating legacy and new metadata repository implementation...");

        legacyRepository.validate();
        newRepository.validate();

        long stop = System.currentTimeMillis();
        getLogger().info("COMPLETE: validation of legacy and new metadata repository in " + (stop - start) + "ms");

    }

    /**
     * Creates a {@link ModelRepositoryConfiguration} utilized by the target
     * {@link ModelInstanceRepository} for metamodel loading. This method
     * primarily extracts the dependencies specified in the
     * {@link FermenterPlugin#TARGET_MODEL_INSTANCES_CONFIGURATION} and
     * {@link FermenterPlugin#METADATA_DEPENDENCIES_CONFIGURATION}
     * configurations and enables them to be appropriately referenced through
     * the created {@link ModelRepositoryConfiguration}.
     *
     * @return appropriately configured {@link ModelRepositoryConfiguration}
     *         that may be used for metamodel processing.
     * @throws MalformedURLException
     */
    protected ModelRepositoryConfiguration createMetamodelRepositoryConfiguration() throws MalformedURLException {
        ModelRepositoryConfiguration config = new ModelRepositoryConfiguration();
        config.setArtifactId(artifactId);
        config.setBasePackage(getBasePackageAsString());

        final List<String> targetedArtifactIds = new ArrayList<>();
        // Retrieve dependencies specified under the "targetModelInstances"
        // configuration and extract their artifact IDs. If no
        // targetModelInstance configuration is specified, add this project's
        // artifactId, which effectively triggers the generation of all targets
        // with a metadataContext of local
        Configuration targetModelInstancesConf = getProject().getConfigurations()
                .getByName(FermenterPlugin.TARGET_MODEL_INSTANCES_CONFIGURATION);
        if (targetModelInstancesConf.getAsFileTree().isEmpty()) {
            getLogger().debug("{} configuration is empty - adding local metadata from {} as generation target",
                    FermenterPlugin.TARGET_MODEL_INSTANCES_CONFIGURATION, artifactId);
            targetedArtifactIds.add(artifactId);
        } else {
            targetModelInstancesConf.getDependencies()
                    .forEach(dependency -> targetedArtifactIds.add(dependency.getName()));
        }

        if (targetedArtifactIds.size() > 1 || !targetedArtifactIds.contains(artifactId)) {
            getLogger()
                    .info(String.format("Generation targets (size=%d) are different from project's local metadata: %s",
                            targetedArtifactIds.size(), targetedArtifactIds.toString()));
        }

        config.setTargetModelInstances(targetedArtifactIds);
        Map<String, ModelInstanceUrl> metamodelUrls = new HashMap<String, ModelInstanceUrl>();
        // Add the current project's src/main/resources as a location where
        // metamodels are defined
        String projectUrl = new File(getMainSourceRootAsFile(), "resources").toURI().toURL().toString();
        metamodelUrls.put(artifactId, new ModelInstanceUrl(artifactId, projectUrl));
        PackageManager.addMapping(artifactId, getBasePackageAsString());

        // Iterate through each of the dependencies specified in the
        // "metadataDependencies" configuration and capture its location such
        // that its contained metamodels may be extracted
        Configuration metadataDependenciesConf = getProject().getConfigurations()
                .getByName(FermenterPlugin.METADATA_DEPENDENCIES_CONFIGURATION);
        if (!metadataDependenciesConf.getAsFileTree().isEmpty()) {
            for (Dependency metamodelDependency : metadataDependenciesConf.getDependencies()) {
                String dependencyArtifactId = metamodelDependency.getName();
                File dependencyFile = metadataDependenciesConf.files(metamodelDependency).iterator().next();
                URL dependencyFileUrl = dependencyFile.toURI().toURL();
                metamodelUrls.put(dependencyArtifactId,
                        new ModelInstanceUrl(dependencyArtifactId, dependencyFileUrl.toString()));
                PackageManager.addMapping(dependencyArtifactId, dependencyFileUrl);
                getLogger().info("Adding metadataDependency {} to current set of metadata", dependencyArtifactId);
            }
        }
        config.setMetamodelInstanceLocations(metamodelUrls);

        return config;
    }

    /**
     * After loading and validating metamodels, generate all sources, resources,
     * etc. by loading all {@link Target}s referenced within the specified
     * {@link org.bitbucket.fermenter.mda.element.Profile} and delegating to the
     * specified {@link Generator}.
     */
    protected void performSourcesGeneration() {
        long start = System.currentTimeMillis();
        String targetProfile = getExtension().getProfile().get();
        ExpandedProfile profile = profiles.get(targetProfile);

        // First validate the profile specified by the developer
        if (profile == null) {
            StringBuilder validProfiles = new StringBuilder();
            for (ExpandedProfile profileValue : profiles.values()) {
                validProfiles.append("\t- ").append(profileValue.getName()).append("\n");
            }

            getLogger().error(String.format(
                    "fermenter { \n\t...\n\tprofile %s  <------ INVALID PROFILE\n\t...\n}\n Profile '%s' is invalid. Please choose one of the following valid profiles:\n%s",
                    targetProfile, targetProfile, validProfiles.toString()));
            throw new InvalidUserDataException(String.format("Invalid profile specified: '%s'", targetProfile));
        } else {
            getLogger().info("Generating code for profile '{}'", profile.getName());
        }

        // For each target, instantiate a generator and call generate
        for (Target target : profile.getTargets()) {
            getLogger().debug("\tExecuting target '{}'", target.getName());

            GenerationContext context = new GenerationContext(target);
            context.setBasePackage(getBasePackageAsString());
            context.setGeneratedSourceDirectory(getExtension().getGeneratedSourceRoot().getAsFile().get());
            context.setMainSourceDirectory(getMainSourceRootAsFile());
            context.setEngine(velocityEngine);
            context.setGroupId(getProject().getGroup().toString());
            context.setArtifactId(artifactId);
            context.setVersion(getProject().getVersion().toString());

            try {
                Class<?> clazz = Class.forName(target.getGenerator());
                Generator generator = (Generator) clazz.newInstance();
                generator.setMetadataContext(target.getMetadataContext());
                generator.generate(context);
            } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
                throw new GradleException(
                        String.format("Could not successfully execute generator for target %s", target.getName()), e);
            }
        }

        long stop = System.currentTimeMillis();
        getLogger().info(String.format("Generation completed in %d ms", (stop - start)));

    }

    /**
     * Creates a
     * {@link org.bitbucket.fermenter.mda.GenerateSourcesHelper.LoggerDelegate}
     * that delegates logging to the logger implementation made available to
     * Gradle {@link org.gradle.api.Task}s.
     */
    protected GenerateSourcesHelper.LoggerDelegate gradleLoggerDelegate = new GenerateSourcesHelper.LoggerDelegate() {
        @Override
        public void log(LogLevel level, String message) {
            Logger logger = getLogger();
            switch (level) {
            case TRACE:
                logger.trace(message);
                break;
            case DEBUG:
                logger.debug(message);
                break;
            case INFO:
                logger.info(message);
                break;
            case WARN:
                logger.warn(message);
                break;
            case ERROR:
                logger.error(message);
                break;
            default:
                logger.info(message);
                break;
            }
        }
    };

    /**
     * Loads all files from the given {@link Configuration} that match the
     * specified resource name and sends them as an {@link InputStream} to the
     * provided {@link Consumer} for processing.
     *
     * @param configuration
     *            desired {@link Configuration} from which to load files.
     * @param resourceName
     *            target resource name to search for.
     * @param loadFromStream
     *            contains the desired logic to process an {@link InputStream}
     *            loaded with the target resource, if any can be found within
     *            the given {@link Configuration}.
     */
    private void loadFromFilesInConfiguration(Configuration configuration, String resourceName,
            Consumer<InputStream> loadFromStream) {
        Set<File> matchingFiles = configuration.getAsFileTree().matching(new PatternSet().include("**/" + resourceName))
                .getFiles();
        for (File file : matchingFiles) {
            try (InputStream stream = new FileInputStream(file)) {
                loadFromStream.accept(stream);
            } catch (IOException e) {
                throw new InvalidUserDataException(String.format("Unable to parse %s", resourceName), e);
            }
        }
    }

    /**
     * Loads all JAR files from the given {@link Configuration}, extract any
     * files that match the specified resource name, and send them as an
     * {@link InputStream} to the given {@link Consumer} for processing.
     *
     * @param configuration
     *            desired {@link Configuration} from which find resources within
     *            included JAR files.
     * @param resourceName
     *            target resource name to search for.
     * @param loadFromStream
     *            contains the desired logic to process an {@link InputStream}
     *            loaded with the target resource, if any can be found within
     *            the JARs included in the given {@link Configuration}.
     */
    private void loadFromJarsInConfiguration(Configuration configuration, String resourceName,
            Consumer<InputStream> loadFromStream) {
        Set<File> jarFiles = configuration.getAsFileTree().matching(new PatternSet().include("**/*.jar")).getFiles();
        for (File jarFile : jarFiles) {
            try (JarFile jar = new JarFile(jarFile)) {
                ZipEntry targetsZipEntry = jar.getEntry(resourceName);
                if (targetsZipEntry == null) {
                    getLogger().info(
                            String.format("Could not find %s configuration in %s", resourceName, jarFile.getPath()));
                } else {
                    loadFromStream.accept(jar.getInputStream(targetsZipEntry));
                }
            } catch (IOException e) {
                throw new InvalidUserDataException(String.format("Unable to extract and parse %s from JAR file %s",
                        resourceName, jarFile.getPath()), e);
            }
        }
    }

}
