package org.bitbucket.cpointe.fermenter.agitator;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;

/**
 * Creates a {@link GenerateSourcesTask} along with needed configurations and
 * delegates metamodel loading, validation, and code generation to Fermenter.
 * This task is automatically injected into the {@link JavaPlugin}'s compilation
 * task graph and all generated sources and resources appropriately added to the
 * relevant {@link SourceSet}s.
 */
public class FermenterPlugin implements Plugin<Project> {

    /**
     * Configuration that aggregates all file references to, or dependencies
     * that contain, targets.json and profiles.json specifications, which
     * specify Fermenter generation targets and profiles respectively.
     */
    public static String PROFILES_AND_TARGETS_CONFIGURATION = "profilesAndTargets";

    /**
     * Configuration that aggregates all dependencies that contain Fermenter
     * metamodels and for Fermenter targets that have a metadataContext of
     * "targeted", execute generation only using these metamodels. If no
     * dependencies are specified for this configuration (which is typical for
     * simple generation scenarios), all Fermenter targets with a
     * metadataContext of "local" will be generated using all available
     * metamodels.
     */
    public static String TARGET_MODEL_INSTANCES_CONFIGURATION = "targetModelInstances";

    /**
     * Configuration that aggregates all dependencies that contain Fermenter
     * metamodels that should be used for generation.
     */
    public static String METADATA_DEPENDENCIES_CONFIGURATION = "metadataDependencies";

    @Override
    public void apply(Project project) {
        project.getPluginManager().apply(JavaPlugin.class);

        FermenterPluginExtension extension = project.getExtensions().create("fermenter", FermenterPluginExtension.class,
                project);
        Provider<GenerateSourcesTask> generateSourcesTask = project.getTasks().register("fermenterGenerateSources",
                GenerateSourcesTask.class, task -> {
                    task.setExtension(extension);
                });

        project.getPlugins().withType(JavaPlugin.class, plugin -> {
            FermenterPluginExtension fermenter = project.getExtensions().getByType(FermenterPluginExtension.class);
            /**
             * Adds the generated source code and resources destination
             * directories to the list of Java source directories such that it
             * can be seamlessly referenced by any code in the module utilizing
             * this plugin
             */
            SourceSetContainer sourceSets = project.getExtensions().getByType(SourceSetContainer.class);
            sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).getJava()
                    .srcDir(fermenter.getGeneratedCompileSourceRoot());
            sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).getResources()
                    .srcDir(fermenter.getGeneratedResourceRoot());
        });

        /**
         * Create the configurations that plugin consumers will use to specify
         * the dependencies that contain Fermenter generation targets/profiles,
         * metamodels, etc.
         */
        project.getConfigurations().maybeCreate(PROFILES_AND_TARGETS_CONFIGURATION).setTransitive(false);
        project.getConfigurations().maybeCreate(TARGET_MODEL_INSTANCES_CONFIGURATION).setTransitive(false);
        project.getConfigurations().maybeCreate(METADATA_DEPENDENCIES_CONFIGURATION).setTransitive(false);

        /**
         * Adds {@link GenerateSourcesTask} as a dependency for the
         * "compileJava" and "processResources" tasks such that code generation
         * is always executed prior to compilation
         */
        project.getTasks().matching(task -> JavaPlugin.COMPILE_JAVA_TASK_NAME.equals(task.getName())
                || JavaPlugin.PROCESS_RESOURCES_TASK_NAME.equals(task.getName())).stream().forEach(task -> {
                    task.dependsOn(generateSourcesTask);
                });
    }
}
