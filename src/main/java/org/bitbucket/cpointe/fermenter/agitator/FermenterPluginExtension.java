package org.bitbucket.cpointe.fermenter.agitator;

import java.io.File;
import java.util.Arrays;

import org.aeonbits.owner.KrauseningConfigFactory;
import org.bitbucket.fermenter.mda.metamodel.DefaultModelInstanceRepository;
import org.bitbucket.fermenter.mda.metamodel.MetamodelConfig;
import org.gradle.api.Project;
import org.gradle.api.file.Directory;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.FileTree;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.*;
import org.gradle.api.tasks.util.PatternSet;

/**
 * Configuration for {@link GenerateSourcesTask} that allows developers to
 * control the target locations at which Fermenter places generated files, as
 * well as which generation {@link org.bitbucket.fermenter.mda.element.Profile}
 * to use.
 */
public class FermenterPluginExtension {

    /**
     * Default location at which non-modifiable Fermenter sources will be
     * generated
     */
    protected static final String DEFAULT_GENERATED_ROOT = "build/generated/fermenter";

    /**
     * Default location at which modifiable Fermenter sources will be generated.
     */
    protected static final String DEFAULT_MAIN_SOURCE_ROOT = "src/main";

    /**
     * Default implementation of Fermenter's metamodel repository to leverage
     * during generation.
     */
    protected static final String DEFAULT_METADATA_REPOSITORY_IMPL = DefaultModelInstanceRepository.class
            .getCanonicalName();

    private static final MetamodelConfig METAMODEL_CONFIG = KrauseningConfigFactory.create(MetamodelConfig.class);

    private Property<String> profile;
    private Property<String> basePackage;
    private DirectoryProperty generatedSourceRoot;
    private DirectoryProperty generatedCompileSourceRoot;
    private DirectoryProperty generatedResourceRoot;
    private DirectoryProperty mainSourceRoot;
    private Property<String> metadataRepositoryImpl;

    /**
     * Creates a new {@link FermenterPluginExtension} configuration container
     * and appropriately instantiates and sets defaults for various
     * configurations.
     * 
     * @param project
     *            parent {@link Project} in which Fermenter is being used.
     */
    public FermenterPluginExtension(Project project) {
        this.profile = project.getObjects().property(String.class);
        this.basePackage = project.getObjects().property(String.class);
        this.generatedSourceRoot = project.getObjects().directoryProperty().fileValue(new File(DEFAULT_GENERATED_ROOT));
        this.generatedCompileSourceRoot = project.getObjects().directoryProperty()
                .fileValue(new File(DEFAULT_GENERATED_ROOT, "java"));
        this.generatedResourceRoot = project.getObjects().directoryProperty()
                .fileValue(new File(DEFAULT_GENERATED_ROOT, "resources"));
        this.mainSourceRoot = project.getObjects().directoryProperty().fileValue(new File(DEFAULT_MAIN_SOURCE_ROOT));
        this.metadataRepositoryImpl = project.getObjects().property(String.class)
                .value(DEFAULT_METADATA_REPOSITORY_IMPL);
    }

    /**
     * Gets the Fermenter generation profile to use, which specifies what
     * artifacts to generate for the provided metamodel.
     * 
     * @return
     */
    @Input
    public Property<String> getProfile() {
        return this.profile;
    }

    /**
     * Sets the Fermenter profile, which must reference a valid profile that is
     * loaded through the
     * {@link FermenterPlugin#PROFILES_AND_TARGETS_CONFIGURATION} configuration,
     * to use for specifying what to generate for the provided metamodels.
     *
     * @param profile
     *            desired profile specifying what to generate.
     */
    public void setProfile(String profile) {
        this.profile.set(profile);
    }

    /**
     * Gets the base package that applicable generation targets (i.e. Java
     * source code) will reference or be generated into.
     * 
     * @return
     */
    @Input
    public Property<String> getBasePackage() {
        return this.basePackage;
    }

    /**
     * Sets the base package for applicable generation targets (i.e. Java source
     * code).
     *
     * @param basePackage
     *            desired package to use for applicable generation targets.
     */
    public void setBasePackage(String basePackage) {
        this.basePackage.set(basePackage);
    }

    /**
     * Gets base location at which generated, modifiable files will be placed.
     * If not set by the developer using this extension, the default
     * {@link #DEFAULT_MAIN_SOURCE_ROOT} will be used.
     *
     * @return
     */
    @OutputDirectory
    public DirectoryProperty getMainSourceRoot() {
        return this.mainSourceRoot;
    }

    /**
     * Sets the base location at which generated, modifiable files will be
     * placed.
     *
     * @param mainSourceRoot
     *            base {@link Directory} at which generated, -modifiable files
     *            will be placed.
     */
    public void setMainSourceRoot(Directory mainSourceRoot) {
        this.mainSourceRoot.set(mainSourceRoot);
    }

    /**
     * Sets the base location at which generated, modifiable files will be
     * placed.
     *
     * @param mainSourceRoot
     *            {@link File} indicating the base directory at which generated,
     *            modifiable files will be placed.
     */
    public void setMainSourceRoot(File mainSourceRoot) {
        this.mainSourceRoot.set(mainSourceRoot);
    }

    /**
     * Gets base location at which generated, non-modifiable files will be
     * placed. If not set by the developer using this extension, the default
     * {@link #DEFAULT_GENERATED_ROOT} will be used.
     *
     * @return
     */
    @OutputDirectory
    public DirectoryProperty getGeneratedSourceRoot() {
        return this.generatedSourceRoot;
    }

    /**
     * Sets the base location at which generated, non-modifiable files will be
     * placed. <b>NOTE:</b> Developers will likely need to also mutate the
     * compile source root (i.e.
     * {@link #setGeneratedCompileSourceRoot(Directory)}) and the resources root
     * (i.e. {@link #setGeneratedResourceRoot(Directory)}) if updating the base
     * generated root directory from the default value.
     * 
     * @param generatedSourceRoot
     *            base {@link Directory} at which generated, non-modifiable
     *            files will be placed.
     */
    public void setGeneratedSourceRoot(Directory generatedSourceRoot) {
        this.generatedSourceRoot.set(generatedSourceRoot);
    }

    /**
     * Sets the base location at which generated, non-modifiable files will be
     * placed. <b>NOTE:</b> Developers will likely need to also mutate the
     * compile source root (i.e. {@link #setGeneratedCompileSourceRoot(File)})
     * and the resources root (i.e. {@link #setGeneratedResourceRoot(File)}) if
     * updating the base generated root directory from the default value.
     *
     * @param generatedSourceRoot
     *            {@link File} indicating the base directory at which generated,
     *            non-modifiable files will be placed.
     */
    public void setGeneratedSourceRoot(File generatedSourceRoot) {
        this.generatedSourceRoot.set(generatedSourceRoot);
    }

    /**
     * Gets the base location at which generated, non-modifiable Java source
     * code will be placed. If not set by the developer using this extension,
     * the default "{@link #DEFAULT_GENERATED_ROOT}/java" will be used.
     *
     * @return base location at which generated, non-modifiable Java source code
     *         will be placed.
     */
    @OutputDirectory
    public DirectoryProperty getGeneratedCompileSourceRoot() {
        return this.generatedCompileSourceRoot;
    }

    /**
     * Sets the base location at which generated, non-modifiable Java source
     * code will be placed. <b>NOTE:</b> Developers will likely need to also
     * mutate the base generated files root (i.e.
     * {@link #setGeneratedSourceRoot(Directory)}) and the resources root (i.e.
     * {@link #setGeneratedResourceRoot(Directory)}) if updating the generated
     * Java source code directory from the default value.
     *
     * @param generatedCompileSourceRoot
     *            base {@link Directory} at which generated, non-modifiable Java
     *            source code will be placed.
     */
    public void setGeneratedCompileSourceRoot(Directory generatedCompileSourceRoot) {
        this.generatedCompileSourceRoot.set(generatedCompileSourceRoot);
    }

    /**
     * Sets the base location at which generated, non-modifiable Java source
     * code will be placed. <b>NOTE:</b> Developers will likely need to also
     * mutate the base generated files root (i.e.
     * {@link #setGeneratedSourceRoot(File)}) and the resources root (i.e.
     * {@link #setGeneratedResourceRoot(File)}) if updating the generated Java
     * source code directory from the default value.
     *
     * @param generatedCompileSourceRoot
     *            {@link File} indicating the base directory at which generated,
     *            non-modifiable Java source code will be placed.
     */
    public void setGeneratedCompileSourceRoot(File generatedCompileSourceRoot) {
        this.generatedCompileSourceRoot.set(generatedCompileSourceRoot);
    }

    /**
     * Gets the base location at which generated, non-modifiable resource files
     * will be placed. If not set by the developer using this extension, the
     * default "{@link #DEFAULT_GENERATED_ROOT}/resources" will be used.
     *
     * @return base location at which generated, non-modifiable resource files
     *         will be placed.
     */
    @OutputDirectory
    public DirectoryProperty getGeneratedResourceRoot() {
        return this.generatedResourceRoot;
    }

    /**
     * Sets the base location at which generated, non-modifiable resource files
     * will be placed. <b>NOTE:</b> Developers will likely need to also mutate
     * the base generated files root (i.e.
     * {@link #setGeneratedSourceRoot(Directory)}) and the compile source root
     * (i.e. {@link #setGeneratedCompileSourceRoot(Directory)}) if updating the
     * generated resource files directory from the default value.
     *
     * @param generatedResourceRoot
     *            base {@link Directory} at which generated, non-modifiable
     *            resource files will be placed.
     */
    public void setGeneratedResourceRoot(Directory generatedResourceRoot) {
        this.generatedResourceRoot.set(generatedResourceRoot);
    }

    /**
     * Sets the base location at which generated, non-modifiable resource files
     * will be placed. <b>NOTE:</b> Developers will likely need to also mutate
     * the base generated files root (i.e.
     * {@link #setGeneratedSourceRoot(Directory)}) and the compile source root
     * (i.e. {@link #setGeneratedCompileSourceRoot(Directory)}) if updating the
     * generated resource files directory from the default value.
     *
     * @param generatedResourceRoot
     *            {@link File} indicating the base directory at which generated,
     *            non-modifiable resource files will be placed.
     */
    public void setGeneratedResourceRoot(File generatedResourceRoot) {
        this.generatedResourceRoot.set(generatedResourceRoot);
    }

    /**
     * Gets a string value of the fully qualified class name of the
     * {@link org.bitbucket.fermenter.mda.metamodel.ModelInstanceRepository}
     * implementation that will be used for metamodel parsing, validation, and
     * loading. If not set by the developer using this extension, the default
     * {@link #DEFAULT_METADATA_REPOSITORY_IMPL} will be used.
     *
     * @return {@link org.bitbucket.fermenter.mda.metamodel.ModelInstanceRepository}
     *         implementation that will be used for metamodel parsing,
     *         validation, and loading.
     */
    @Input
    public Property<String> getMetadataRepositoryImpl() {
        return this.metadataRepositoryImpl;
    }

    /**
     * Sets the
     * {@link org.bitbucket.fermenter.mda.metamodel.ModelInstanceRepository}
     * implementation that will be used for metamodel parsing, validation, and
     * loading. <b>NOTE:</b> Typically, developers won't need to configure this
     * value.
     * 
     * @param metadataRepositoryImpl
     *            string value capturing the fully qualified class name of the
     *            desired
     *            {@link org.bitbucket.fermenter.mda.metamodel.ModelInstanceRepository}
     *            implementation to use.
     */
    public void setMetadataRepositoryImpl(String metadataRepositoryImpl) {
        this.metadataRepositoryImpl.set(metadataRepositoryImpl);
    }

    /**
     * Read-only property that captures a {@link FileTree} of all locally
     * defined metamodels, enabling source generation to be skipped if no
     * metamodels have been updated.
     *
     * @return {@link FileTree} of all locally defined metamodels.
     */
    @InputFiles
    @SkipWhenEmpty
    @PathSensitive(PathSensitivity.ABSOLUTE)
    public FileTree getLocalMetamodelDefinitions() {
        PatternSet metamodelPatterns = new PatternSet();
        for (String relativePath : Arrays.asList(METAMODEL_CONFIG.getDictionaryTypesRelativePath(),
                METAMODEL_CONFIG.getEntitiesRelativePath(), METAMODEL_CONFIG.getEnumerationsRelativePath(),
                METAMODEL_CONFIG.getServicesRelativePath())) {
            metamodelPatterns = metamodelPatterns.include(String.format("resources/%s/*.json", relativePath));
        }
        return getMainSourceRoot().getAsFileTree().matching(metamodelPatterns);
    }
}
