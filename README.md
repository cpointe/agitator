# Agitator #
[![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://opensource.org/licenses/mit)

In brewing, [agitation](http://brulosophy.com/2016/10/17/the-impact-of-continuous-agitation-during-fermentation-exbeeriment-results/)
has been shown to improve fermentation speed. In the world of build automation, using Gradle may improve build performance
and Agitator allows developers to speedily apply [Fermenter's](https://bitbucket.org/cpointe/fermenter) model-driven
architecture concepts to their Gradle builds!

## Usage ##

Apply the plugin in your buildscript:
```
plugins {
  id 'org.bitbucket.cpointe.fermenter.agitator' version '2.0.0'
}
```
Configure the plugin to specify the desired `basePackage` that will be used to differentiate and namespace generated files,
the target generation `profile` that defines what will be generated, and other configurations related to metamodel processing.
For example:
```
fermenter {
  // *REQUIRED* - base package for applicable generation targets (i.e. Java source files)
  basePackage 'com.foo.baz'

  // *REQUIRED* - name of the Fermenter profile to use, which specifies what files to generate
  // for the provided metamodel. This value *must* reference a valid profile that is included
  // in the artifacts specified in the profilesAndTargets dependency configuration
  profile 'business-objects-and-services'
}
dependencies {
  // *REQUIRED* - Dependency configuration created by Agitator that allows developers to specify
  // the files, dependencies, and other artifacts in which Fermenter generation profiles and
  // targets definitions exist.  Profile and target definitions *must* be named profiles.json and
  // targets.json respectively. As seen below, profiles.json and targets.json definitions may be
  // bundled into Gradle/Maven dependencies or referenced directly as files.
  profilesAndTargets 'org.bitbucket.askllc.fermenter.stout:stout-spring-mda:2.0.0'
  profilesAndTargets files('src/main/resources/profiles.json')

  // Optional - Dependency configuration created by Agitator that enables developers to specify
  // any additional dependency artifacts in which metamodels that are *not* locally defined in
  // src/main/resources may be found
  metadataDependencies 'com.foo.baz:bar-metamodel:1.0.0'
  metadataDependencies 'com.foo.baz:foo-metamodel:1.0.0'

  // Optional - Dependency configuration created by Agitator that enables developers to select
  // dependency artifacts that contain metamodels that will be applied to generation targets
  // that have a metadataContext value of 'targeted'.  If not artifacts are specified, locally
  // defined metamodels will be applied to generation targets that have a metadataContext value
  // of 'local'.
  targetModelInstances 'com.foo.baz:bar-metamodel:1.0.0'

  // Don't forget to add any Fermenter dependencies required by generated code!
  implementation 'org.bitbucket.askllc.fermenter.stout:stout-spring:2.0.0'
  implementation 'org.bitbucket.askllc.fermenter.stout:stout-authzforce:2.0.0'
}
```
The configurations exposed by Agitator align with those leveraged by the Fermenter Maven plugin. See the 
[following](https://fermenter.atlassian.net/wiki/spaces/FER/pages/48955396/Controlling+What+Gets+Generated#ControllingWhatGetsGenerated-Specifyingtheconfigurationblockoftheplugin)
for more details on each configuration option and examples.

In order to ensure that developers can easily integrate Agitator into their build, Agitator automatically adds the
`fermenterGenerateSources` task as a dependent of the [compileJava](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.compile.JavaCompile.html)
and `processResources` tasks and adds the appropriate generated Java source code directories to the appropriate `java` [SourceSet](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.SourceSet.html).

### Advanced Usage Scenarios ###

Any custom Fermenter generators or generation targets and profiles definitions that are not provided through Fermenter's core modules (`stout`, `ale`, `lambic`) should
be included via `buildscript` `dependencies`.  For example:
```
buildscript {
  dependencies {
    classpath 'com.foo.baz:custom-fermenter-generators-and-targets:1.0.0'
  }
}
```
If developers want to mitigate the need for separately publishing modules for all custom Fermenter extension logic in order to
ensure that they are available for inclusion as `buildscript` dependencies, developers should consider using the project's
`buildSrc` capability.  For example, consider that we want to define a custom `targets.json`, `profiles.json`, and new generator
called `FooBarGenerator` that is used within that `targets.json` definition:
```
buildSrc/src/main
│
└───java
│   │ com/foo/bar/FooBarGenerator.java
│   │
└───resources
│   │   profiles.json
│   │   targets.json
│   │
│   └───templates
│       │   important-class-to-generate.java.vm
```
Inside the `targets.json` configuration, the new custom `FooBarGenerator` and `important-class-to-generate.java.vm`
template may be referenced:
```
 [
   {
 	"name": "importantClassForFooBar",
 	"templateName": "templates/important-class-to-generate.java.vm",
 	"outputFile": "${basePackage}/ImportantClass.java",
 	"generator": "com.foo.bar.FooBarGenerator",
 	"metadataContext": "local",
 	"overwritable": false
   }
]
```
The custom `profiles.json` may create new profiles that rely on this new `importantClassForFooBar` generation target. In
order to enable Agitator to process these custom `targets.json` and `profiles.json` definitions, use the `profilesAndTargets`
configuration:
```
dependencies {
  // Load the targets.json and profiles.json from buildSrc. The FooBarGenerator class
  // will be automatically added to the buildscript classpath
  profilesAndTargets files("$rootDir/buildSrc/src/main/resources/targets.json")
  profilesAndTargets files("$rootDir/buildSrc/src/main/resources/profiles.json")

  // Note how multiple custom targets.json and profiles.json may be concurrently used
  profilesAndTargets files('src/main/resources/profiles.json')

  // Still use the Fermenter generation functionality provided by Stout
  profilesAndTargets 'org.bitbucket.askllc.fermenter.stout:stout-spring-mda:2.0.0'
}
```
As an alternative `buildSrc`, composite builds may be used to provide the same desired end result, as described [here](https://proandroiddev.com/stop-using-gradle-buildsrc-use-composite-builds-instead-3c38ac7a2ab3).

## Releasing ##
Log into the [Gradle Plugin Repository](https://plugins.gradle.org/), navigate to the `API Keys` section of your user 
profile, and ensure that your publishing key and secret are correctly configured in your local `gradle.properties` file.

Update the `version` in `build.gradle` and execute the `publishPlugins` task:
```
$ ./gradlew publishPlugins
```

As Agitator will likely be updated alongside Fermenter, the version of Agitator should match the version of Fermenter on
which Agitator depends.

## Licensing ##
This plugin is available under the [MIT License](http://opensource.org/licenses/mit-license.php).
